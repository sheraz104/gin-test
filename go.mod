module gitlab.com/sheraz104/gin-test

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.5.0
	gopkg.in/go-playground/validator.v9 v9.29.1
)
