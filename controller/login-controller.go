package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/sheraz104/gin-test/dto"
	"gitlab.com/sheraz104/gin-test/service"
)

type LoginController interface {
	Login(ctx *gin.Context) string
}

type loginController struct {
	loginService service.LoginService
	jwtService service.JWTService
}


func NewLoginController(
	loginService service.LoginService,
	jwtService service.JWTService,
) LoginController {
	return &loginController{
		loginService: loginService,
		jwtService:   jwtService,
	}
}

func (controller loginController) Login(ctx *gin.Context) string {
	var creds dto.Credentials
	err := ctx.ShouldBind(&creds)
	if err != nil {
		return ""
	}

	authenticated := controller.loginService.Login(creds.Username, creds.Password)
	if authenticated {
		return controller.jwtService.GenerateToken(creds.Username, true)
	}
	return ""
}
