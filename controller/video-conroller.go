package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/sheraz104/gin-test/entity"
	"gitlab.com/sheraz104/gin-test/service"
	"gitlab.com/sheraz104/gin-test/validators"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
)

type VideoController interface {
	Save(ctx *gin.Context) error
	GetAll() []entity.Video
	Show(ctx *gin.Context)
}

type controller struct {
	videoService service.VideoService
}

var validate *validator.Validate

func New(videoService service.VideoService) VideoController {
	validate = validator.New()
	validate.RegisterValidation("is-cool", validators.ValidatorCool)

	return controller{
		videoService: videoService,
	}
}

func (videoController controller) Save(ctx *gin.Context) error {
	var video entity.Video
	err := ctx.ShouldBindJSON(&video)
	if err != nil {
		return err
	}
	err = validate.Struct(video)
	if err != nil {
		return err
	}
	videoController.videoService.Save(video)
	return nil
}

func (videoController controller) GetAll() []entity.Video {
	return videoController.videoService.GetAll()
}

func (videoController controller) Show(ctx *gin.Context) {
	ctx.HTML(
		http.StatusOK,
		"home.html",
		gin.H{},
	)
}
