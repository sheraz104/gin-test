package service

type LoginService interface {
	Login(username, password string) bool
}

type loginService struct {
	authorizeUsername, authorizedPassword string
}

func NewLoginService() LoginService {
	return &loginService{
		authorizeUsername: "sheraz",
		authorizedPassword: "admin",
	}
}

func (service loginService) Login(username, password string) bool  {
	return service.authorizeUsername == username &&
		service.authorizedPassword == password
}