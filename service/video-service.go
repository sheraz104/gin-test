package service

import "gitlab.com/sheraz104/gin-test/entity"

type VideoService interface {
	Save(video entity.Video) entity.Video
	GetAll() []entity.Video
}

type service struct {
	videos []entity.Video
}

func New() VideoService {
	return &service{}
}

func (service *service) Save(video entity.Video) entity.Video {
	service.videos = append(service.videos, video)
	return video
}

func (service *service) GetAll() []entity.Video {
	return service.videos
}