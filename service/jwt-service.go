package service

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"os"
	"time"
)

type JWTService interface {
	GenerateToken(name string, admin bool) string
	ValidateToken(tokenStr string) (*jwt.Token, error)
}

type jwtCustomClaims struct {
	username string `json:"username"`
	admin bool `json:"admin"`
	jwt.StandardClaims
}

type jwtService struct {
	secret, issuer string
}

func NewJWTService() JWTService {
	return &jwtService{
		secret: getSecret(),
		issuer: "sheraz arshad",
	}
}

func getSecret() string {
	secret := os.Getenv("JWT_SECRET")
	if secret == "" {
		secret = "my_secret"
	}
	return secret
}

func (service jwtService) GenerateToken(username string, admin bool) string {
	claims := &jwtCustomClaims{
		username,
		admin,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			Issuer:    service.issuer,
			IssuedAt:  time.Now().Unix(),

		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err := token.SignedString([]byte(service.secret))
	if err != nil {
		panic(err)
	}
	return tokenStr
}

func (service jwtService) ValidateToken(tokenStr string) (*jwt.Token, error) {
	return jwt.Parse(tokenStr, func(token *jwt.Token) (i interface{}, err error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(service.secret), nil
	})
}