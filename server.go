package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/sheraz104/gin-test/controller"
	"gitlab.com/sheraz104/gin-test/middlewares"
	"gitlab.com/sheraz104/gin-test/service"
	"io"
	"net/http"
	"os"
)

var (
	videoService   service.VideoService       = service.New()
	loginService service.LoginService = service.NewLoginService()
	jwtService service.JWTService = service.NewJWTService()

	videoController controller.VideoController = controller.New(videoService)
	loginController controller.LoginController = controller.NewLoginController(
		loginService,
		jwtService,
	)
)

func main() {
	setupLogOutput()

	server := gin.New()
	server.Use(
		gin.Recovery(),
		middlewares.Logger(),
	)
	server.LoadHTMLGlob("templates/*.html")
	server.POST("/login", func(context *gin.Context) {
		token := loginController.Login(context)
		if token != "" {
			context.JSON(http.StatusOK, gin.H{
				"token": token,
			})
			return
		}
		context.JSON(http.StatusUnauthorized, gin.H{
			"err": "not authorized",
		})
	})
	apiRoutes := server.Group("/api", middlewares.AuthorizeJWT())
	{
		apiRoutes.GET("/", func(context *gin.Context) {
			context.JSON(http.StatusOK, videoController.GetAll())
		})
		apiRoutes.POST("/", func(context *gin.Context) {
			err := videoController.Save(context)
			if err != nil {
				context.JSON(http.StatusBadRequest, gin.H{
					"error": err.Error(),
				})
			} else {
				context.JSON(http.StatusOK, gin.H{
					"message": "video input is valid and saved",
				})
			}
		})
	}
	
	viewRoutes := server.Group("/view")
	{
		viewRoutes.GET("/", videoController.Show)
	}
	

	server.Run(":8080")
}

func setupLogOutput() {
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}
