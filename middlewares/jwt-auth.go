package middlewares

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/sheraz104/gin-test/service"
	"log"
	"net/http"
)

func AuthorizeJWT() gin.HandlerFunc {
	return func(context *gin.Context) {
		const BEARER_SCHEMA string = "Bearer "
		authHeader := context.GetHeader("Authorization")
		tokenStr := authHeader[len(BEARER_SCHEMA):]

		token, err := service.NewJWTService().ValidateToken(tokenStr)

		if token.Valid {
			claims := token.Claims.(jwt.MapClaims)
			fmt.Println("Name: ", claims)
			fmt.Println("IsAdmin: ", claims["admin"])
		} else {
			log.Println(err)
			context.AbortWithStatus(http.StatusUnauthorized)
		}
	}
}
